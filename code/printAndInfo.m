function [ blur, mse, ssim, thissnr ] = printAndInfo( recon, cspace, groundTruth , savepath,titleForSaveAndPlot)
%printAndInfo gets blur mse ssim and snr information about a
%reconstruction for subject MID227, will need to replace the coords
%(prbcords_MID227) for SNR calc if using another subject.  also saves
%sampling information and final image as high quality PDFs.


blur = blurMetric(mat2gray(abs(recon)));
mse = norm(abs(recon(:))-abs(groundTruth(:)));
ssim = get_ssim(abs(recon),abs(groundTruth),20);

load pbcords_MID227
thissnr  = mySNR(abs(recon), 1, xFG, yFG, xBG, yBG);

aaa = figure;
imagesc(abs(rot90(rot90((recon(:,:,20))))))
colormap(gray)
brighten(.5)
axis off
set(aaa, 'Color', 'none')
%title(sprintf('%s reconstruction',titleForSaveAndPlot ),'interpreter','none')
%exportfig(aaa,strcat(savepath,titleForSaveAndPlot,'_recon.pdf'),'Renderer','painters','Height',2);
export_fig(strcat(savepath,titleForSaveAndPlot,'_recon'), '-a1', '-pdf','-q100','-painters');

pause(.00001)
close 

bbb = figure;
imagesc(squeeze(cspace(200,:,:)))
colormap(jet)
colorbar 
set(bbb, 'Color', 'none')
axis off
%title(sprintf('%s Sampling Pattern',titleForSaveAndPlot ),'interpreter','none')
%exportfig(bbb,strcat(savepath,titleForSaveAndPlot,'_samps.pdf'),'Renderer','painters')
export_fig(strcat(savepath,titleForSaveAndPlot,'_samps'), '-a1', '-pdf','-q100','-painters');

pause(.00001)
close


end

