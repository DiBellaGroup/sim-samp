function [samp, cont] = randGaussSamp_overlap(cspace,params)
%randGaussSamp_overlap A central weighted gaussian sampling pattern that
%WILL sample the same place twice.

global ssn msn tsm;

if params.fixrng == 1
    rng(ssn)
elseif params.fixrng ==2 
    rng(msn)
else
end


ks = params.ks;
samp = zeros(ks);

while sum(sum(sum(samp))) < params.spb*ks(1)
    
    rng1 = round(ks(2)/2+(sqrt(ks(2)*(params.wid))*randn(1)));
    rng2 = round(ks(3)/2+(sqrt(ks(3)*(params.wid))*randn(1)));
    
    if rng1 >=1 && rng1 <= ks(2) && rng2 >=1 && rng2 <= ks(3)
        samp(:,rng1,rng2) = true;
    end
end

if params.shift
    samp = ifftshift(samp);
else

end

% if length(cspace(:)) == sum(sum(sum(cspace)))
%     cont = 1;
% else
%     cont = 0;
% end

%maybe we just want to always keep going? this is up to the user.
cont = 1;


end

