function [samp, cont] = regSamp(cspace,params)
%REGSAMP regular old cartesian sampling by column.  Specific for LGE as it
%samples normal to major axis of patiant movement.

global ssn msn tsm

if params.fixrng == 1
    rng(ssn)
elseif params.fixrng ==2 
    rng(msn)
else
end


ks = params.ks;
spb = params.spb;

samp = zeros(ks);
samples_per_line = ks(2) / params.spb;

curCol = ceil(ssn / samples_per_line);
curRow = mod (ssn , samples_per_line);

sampRange = 1:samples_per_line:ks(2);

samp(:,sampRange+curRow,curCol) = true;


sum(sum(sum(samp)))
%if (curCol >= params.ks(3) && curRow >= 5)
if (ssn == 354)
    cont = 0;
else
    cont = 1;

end

