function [toRet, cont] = hacked_randGaussSamp_noverlap(cspace,params)
%hacked_randGaussSamp_noverlap A central weighted gaussian sampling pattern that
%will not sample the same place twice.  This one uses a prebaked sampling
%pattern, unlike the other which is acutally generated on the fly.

global ssn msn tsm 

if params.fixrng == 1
    rng(ssn)
elseif params.fixrng ==2 
    rng(msn)
else
end



load('recording')
ks = params.ks;
samp = myRecording(:,:,ssn);
bigsamp = repmat(samp,[1,1,ks(1)]);

if params.shift
    toRet = ifftshift(permute(bigsamp,[3 1 2]));
else
    toRet = (permute(bigsamp,[3 1 2]));
end


%if sum(curCspace(:)) < ks(2)*ks(3) - params.spb

if (ssn == 354)
    cont = 0;
else
    cont = 1;

end

