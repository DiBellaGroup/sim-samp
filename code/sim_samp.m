function [sspace cspaceToRet] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo)
%sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo)
% for usage refer to PDF
global ssn msn tsm i

takePics = 0;
N = length(x);
t = 1:N;
ks = size(kspace_nav);
kspace = kspace_nav;
sspace = zeros(ks);
in_aquis = zeros(ks);
out_aquis = zeros(ks);
cspace = zeros(ks);

ypsmat = ones(ks);
xpsmat = ones(ks);
zpsmat = ones(ks);

i = 1;
cont = 1;
ssn = 1;
msn = 1;
tsm = 1;
piccount = 1;

recordingCspace = zeros(ks(2),ks(3),N);
while cont
    
    %%%%
    % static sample number (non moving, in gate)
    % moving sample number (out of gate)
    % these are used by the sampling algos to keep track of where they
    % should be
    %%%%%
    
    ssn;
    msn;
    
    %%%%
    % shifts
    %%%%%
    
    %%%%
    % with jitter, this would add a small jitter in the non-major axis
    %exp((1:1:size(curCircImg,2))*1i*randn(1)/jitter);
    %%%%
    yps(:,1,1) = exp(  (1:1:ks(1)).*(1i*y(i)) );
    xps(1,:,1) = exp(  (1:1:ks(2)).*(1i*x(i)) );
    zps(1,1,:) = exp(  (1:1:ks(3)).*(1i*z(i)) );
    ypsmat = repmat(yps,[1,ks(2),ks(3)]);
    xpsmat = repmat(xps,[ks(1),1,ks(3)]);
    zpsmat = repmat(zps,[ks(1),ks(2),1]);
    kspace = kspace_nav.*ypsmat.*xpsmat.*zpsmat + sig*complex(randn(ks),randn(ks));
    
    
    %%%%
    % are we in gate or not?
    %%%%%
    if gate_algo(gate_params)
        
    %%%%
    % get samples from ingate_algo
    %%%%%
        [samp,cont] = ingate_algo(in_aquis,ingate_params);
        in_aquis = in_aquis + samp;
        sspace(logical(samp)) = sspace(logical(samp)) + kspace(logical(samp));
        %fprintf('ingate (nsamps %f) \n',sum(sum(sum(samp))));
        
        ssn = ssn +1;
        tsm = tsm +1;
        
    else
        
    %%%%
    % get samples from out of gate algo
    %%%%%
    
        [samp,cont] = outofgate_algo(out_aquis,outofgate_params);
        
    %%%%
    % shift correction a la Ak�akaya et. al. 2010, the hacky version
    %%%%%
        if applyCorrection

            zps(1,1,:) = exp(  (1:1:ks(3)).*(1i*-(z(i)+(randn*0.003))) );
            zpsmat = repmat(zps,[ks(1),ks(2),1]);
            
            %end
            out_aquis = out_aquis + samp;
            kspace_deshift = kspace.*zpsmat;
            sspace(logical(samp)) = sspace(logical(samp)) + kspace_deshift(    logical(samp));
            
            %fprintf('out of gate corrected (nsamps %f) \n',sum(sum(sum(samp))));
        else
            out_aquis = out_aquis + samp;
            sspace(logical(samp)) = sspace(logical(samp)) + kspace(    logical(samp));
            
            %fprintf('out of gate not corrected (nsamps %f) \n',sum(sum(sum(samp))));
        end
        
        
        msn = msn +1;
        tsm = tsm +1;
        
        
    end
    
    %%%%
    % combine the in and out of gate acquisitions
    %%%%%
    
    cspace = out_aquis+in_aquis;

    
    
    if displayRunningInfo
        figure(1)
        h = imagesc(squeeze(cspace(1,:,:)));
        colormap(jet);
        
        
        if takePics
            saveas(h, strcat('./gif/sampPat',num2str(piccount)), 'png');
            piccount = piccount+1;
        end
        
        figure(2)
        plot(t,y,t,x,t,z)
        hold on
        scatter(t(i),y(i))
        hold off
        
        figure(3)
        workingRecon = ifftshift(ifftn(kspace));
        imagesc(abs(workingRecon(:,:,20)));
        colormap(gray);
        
        pause(.00001)
    end
    
    if size(cspace) ~= size(kspace_nav)
        error('myApp:argChk', 'Trying to sample outside of kspace, somthing might be wrong.  Check your ending conditions')
    end

    %%%%
    % move along the navigator signal loop if we reach the end.
    %%%%%
    if i == N;  i = 1; else i = i+1; end;
end

    %%%%
    % average out places where samples have been taken more then once
    %%%%%
sspace( cspace ~= 0 ) = sspace(cspace ~= 0)./cspace(cspace ~= 0);
cspaceToRet = cspace;

end
