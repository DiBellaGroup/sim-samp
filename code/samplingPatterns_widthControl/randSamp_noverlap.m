function [samp, cont] = randSamp_noverlap(cspace,params)
%HOLDSAMP Summary of this function goes here
%   Detailed explanation goes here
global ssn msn tsm 

if params.fixrng == 1
    rng(ssn)
elseif params.fixrng ==2 
    rng(msn)
else
end


ks = params.ks;
samp = zeros(ks);

curCspace = squeeze(cspace(1,:,:));
toSampMat = zeros(size(curCspace));

notYetSamped =  curCspace == 0;
ind = find(notYetSamped);
randpermofsamped  = randperm(length(ind));
toSamp = ind(randpermofsamped(1:33));
toSampMat(toSamp) = true;
samp = repmat(toSampMat,[1,1,ks(1)]);

if params.shift
    samp = ifftshift(permute(samp,[3,1,2]));
else
    samp = permute(samp,[3,1,2]);

end


if (ssn == 354)
    cont = 0;
else
    cont = 1;
    


end

