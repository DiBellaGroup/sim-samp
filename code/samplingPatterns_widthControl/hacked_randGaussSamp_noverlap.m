function [toRet, cont] = hacked_randGaussSamp_noverlap(cspace,params)
%HOLDSAMP Summary of this function goes here
%   Detailed explanation goes here
global ssn msn tsm 

if params.fixrng == 1
    rng(ssn)
elseif params.fixrng ==2 
    rng(msn)
else
end



load('recording')
ks = params.ks;
samp = myRecording(:,:,ssn);
bigsamp = repmat(samp,[1,1,ks(1)]);

if params.shift
    toRet = ifftshift(permute(bigsamp,[3 1 2]));
else
    toRet = (permute(bigsamp,[3 1 2]));
end


%if sum(curCspace(:)) < ks(2)*ks(3) - params.spb

if (ssn == 354)
    cont = 0;
else
    cont = 1;

end

