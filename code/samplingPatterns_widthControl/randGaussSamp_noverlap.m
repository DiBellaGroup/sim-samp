function [toRet, cont] = randGaussSamp_noverlap(cspace,params)
%HOLDSAMP Summary of this function goes here
%   Detailed explanation goes here
global ssn msn tsm;

if params.fixrng == 1
    rng(ssn)
elseif params.fixrng ==2 
    rng(msn)
else
end

if params.shift
    curCspace = fftshift(squeeze(cspace(1,:,:)));
else
    curCspace =(squeeze(cspace(1,:,:)));
end

samp = zeros(size(curCspace));



ks = params.ks;
%samp = zeros(ks);

while sum(sum(samp)) < params.spb
    
    rng1 = round(ks(2)/2+(sqrt(ks(2)*(params.wid1))*randn(1)));
    rng2 = round(ks(3)/2+(sqrt(ks(3)*(params.wid2))*randn(1)));
    
    if rng1 >=1 && rng1 <= ks(2) && rng2 >=1 && rng2 <= ks(3) && samp(rng1,rng2) == 0
        
        if curCspace(rng1,rng2) == 0
            
            samp(rng1,rng2) = true;
        else
            
            maskedDistMat = ones(size(curCspace));
            onlycurrent = zeros(size(curCspace));
            onlycurrent(rng1,rng2) = 1;
            %notYetSamped = curCspace == 1;
            [distMat,~] = bwdist(onlycurrent);
            maskedDistMat(~((curCspace == 0).*(samp==0))) = nan;
            maskedDistMat = maskedDistMat.*distMat;
            [row,col] = find(maskedDistMat == min(maskedDistMat(:)),1);
            samp(row,col) = true;
        end
    end
    
    if ~isequal(size(samp),[198 59])
        keyboard
    end
    %size(samp)
    %sum(sum(samp))
    
    
    
end
bigsamp = repmat(samp,[1,1,ks(1)]);

if params.shift
    toRet = ifftshift(permute(bigsamp,[3 1 2]));
else
    toRet = (permute(bigsamp,[3 1 2]));
end
%if sum(curCspace(:)) < ks(2)*ks(3) - params.spb

if (ssn == 354)
    cont = 0;
else
    cont = 1;
    

end

