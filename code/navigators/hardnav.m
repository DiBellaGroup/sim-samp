function [ inthegate ] = hardnav(params)
%HARDNAV(params) this function simply determines using gate_params if the
% current kspace is in or out of gate


global i

if params.z(i) >= 0
    inthegate = 0;
else
    inthegate = 1;
end

end

