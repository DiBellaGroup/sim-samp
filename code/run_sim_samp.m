clear all
close all
clc

savepath = % This path is where the program will save images.

addpath('filter\','data\','navigators\','samplingPatterns\','displayAndMetrics\')

load data.mat
load gy_index_cent

groundTruth = ifftshift(ifftn(kspace_nav));
imagesc(abs(rot90(rot90((groundTruth(:,:,20))))));
colormap(gray)
brighten(.5)

N = length(gy_index_cent);
%N =50;
t = 1:N;
f = .5;
%y = -normpdf(t,N/2,N/50);
%y = -sin(2*f*pi*t);
y = randn(size(t))*.001;
z = (gy_index_cent-1)*.6; %sin(2*f*pi*t);
x = randn(size(t))*.001; %sin(2*f*pi*t);
% plot(t,y)
% figure
% plot(t,z)

sig = 1e-7;
applyCorrection = 0;
displayRunningInfo =0 ;

ingate_params.fixrng = 1
outofgate_params.fixrng = 1
ingate_params.shift = 1
outofgate_params.shift = 1
ingate_params.ks = size(kspace_nav);
ingate_params.spb = 33;
ingate_params.wid = 6;

outofgate_params.ks = size(kspace_nav);
outofgate_params.spb = 33;
outofgate_params.wid = 6;

gate_params.y = y;
gate_params.x = x;
gate_params.z = z;

gate_algo = @hardnav;


dataCollection = [];
titles = {}

%%
ingate_algo = @regSamp;
outofgate_algo = @randGaussSamp_overlap;
titleForSaveAndPlot = 'centric_centrichold_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;




%%%%%%%%%%%%%%%%%%%%%
%%

% ingate_algo = @hacked_randGaussSamp_noverlap;
% outofgate_algo = @randGaussSamp_overlap;
%titleForSaveAndPlot = 'ifftrgsnover6_ifftrgsover6_avg_nocor';



%%%%%%%% AVG AND NAV
ingate_algo = @regSamp;
outofgate_algo = @holdSamp;
titleForSaveAndPlot = 'centric_centrichold_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @regSamp;
outofgate_algo = @ignoreSamp;
titleForSaveAndPlot = 'centric_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


%%
%%%%%%%% TO SHIFT OR NOT TO SHIFT
ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.wid = 4;
ingate_params.wid = 4;
outofgate_params.shift = 1;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_ifftrgsover4_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 1;
ingate_params.shift = 0;
titleForSaveAndPlot = 'rgsnover4_ifftrgsover4_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 0;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_rgsover4_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 0;
ingate_params.shift = 0;
titleForSaveAndPlot = 'rgsnover4_rgsover4_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

%%


%%%%%%%% DOES NOVERLAP WORK BETTER THEN NAV
ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
outofgate_params.shift = 0;
ingate_params.shift = 0;
titleForSaveAndPlot = 'rgsnover4_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
outofgate_params.shift = 0;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


%%%%%%%% NOVERLAP WIDTH
ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 1;
titleForSaveAndPlot = 'ifftrgsnover1_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 2;
titleForSaveAndPlot = 'ifftrgsnover2_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 3;
titleForSaveAndPlot = 'ifftrgsnover3_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 4;
titleForSaveAndPlot = 'ifftrgsnover4_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 5;
titleForSaveAndPlot = 'ifftrgsnover5_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 6;
titleForSaveAndPlot = 'ifftrgsnover6_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 7;
titleForSaveAndPlot = 'ifftrgsnover7_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 8;
titleForSaveAndPlot = 'ifftrgsnover8_centricignore_ignore_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;



%%%%%%%% CENTRIC + RGS (SHIFT?)
ingate_algo = @regSamp;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 1;
ingate_params.shift = 0;
titleForSaveAndPlot = 'centric_ifftrgsnover4_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @regSamp;
outofgate_algo = @randGaussSamp_overlap;
ingate_params.shift = 0;
outofgate_params.wid = 6;
titleForSaveAndPlot = 'centric_ifftrgsnover6_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


ingate_algo = @regSamp;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 0;
ingate_params.shift = 0;
titleForSaveAndPlot = 'centric_rgsover4_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;



%%%%%%%% HACKED RGS WITH CENTRIC?
ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @holdSamp;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_centrichold_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @holdSamp;
ingate_params.shift =0 ;
titleForSaveAndPlot = 'rgsnover4_centrichold_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


%%%%%%%% widths of outofgate testing
ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.wid = 2;
ingate_params.wid = 2;
outofgate_params.shift = 1;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover2_ifftrgsover2_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.wid = 3;
ingate_params.wid = 3;
titleForSaveAndPlot = 'ifftrgsnover3_ifftrgsover3_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.wid = 4;
ingate_params.wid = 4;
titleForSaveAndPlot = 'ifftrgsnover4_ifftrgsover4_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.wid = 5;
ingate_params.wid = 5;
titleForSaveAndPlot = 'ifftrgsnover5_ifftrgsover5_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.wid = 6;
ingate_params.wid = 6;
titleForSaveAndPlot = 'ifftrgsnover6_ifftrgsover6_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.wid = 7;
ingate_params.wid = 7;
titleForSaveAndPlot = 'ifftrgsnover7_ifftrgsover7_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.wid = 8;
ingate_params.wid = 8;
titleForSaveAndPlot = 'ifftrgsnover8_ifftrgsover8_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


%%%%%PLAY
for inv = 0:1
    for inv2 = 0:1
        for i = 1:1:8
            for j = 1:1:8
                applyCorrection = 0;
                ingate_algo = @randGaussSamp_noverlap;
                outofgate_algo = @randGaussSamp_overlap;
                outofgate_params.wid = j;
                ingate_params.wid = i;
                outofgate_params.shift = inv;
                ingate_params.shift = inv2;
                titleForSaveAndPlot = strcat('ifft_',num2str(ingate_params.shift),'_rgsnover_',num2str(ingate_params.wid),'_ifft',num2str(outofgate_params.shift),'rgsover_',num2str(outofgate_params.wid ),'_avg_nocor');
                [sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
                recon1 = ifftshift(ifftn(sspace17));
                [ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
                dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
                titles{end+1} = titleForSaveAndPlot;
                
            end
        end
    end
end




outofgate_params.shift = 0;
ingate_params.shift = 0;
outofgate_params.wid =2;
ingate_params.wid = 2;
ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
titleForSaveAndPlot = 'rgsnover2_ignore_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


ingate_algo = @regSamp;
outofgate_algo = @ignoreSamp;
titleForSaveAndPlot = 'centric_ignore_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.shift = 0;
ingate_params.shift = 0;
ingate_algo = @randSamp_noverlap;
outofgate_algo = @ignoreSamp;
titleForSaveAndPlot = 'rsnover_ignore_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

outofgate_params.shift = 1;
ingate_params.shift = 1;
ingate_algo = @randSamp_noverlap;
outofgate_algo = @ignoreSamp;
titleForSaveAndPlot = 'ifftrsnover_ignore_avg_nocor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;






%%%%%%%% PLOT
metrics =  {'blur','mse','ssim','samples'}

for i=1:1:size(dataCollection,1)
    figure
    bar(dataCollection(i,:))
    set(gca, 'XTickLabel',titles, 'XTick',1:numel(titles))
    set(gca,'defaulttextinterpreter','none')
    rotateXLabels( gca, 90 )
    title(metrics{i})
end

[C,I] = max(dataCollection(1,:))
titles(I)

[C,I] = min(dataCollection(2,:))
titles(I)

[C,I] = max(dataCollection(3,:))
titles(I)




%%

%
%
% clear all
% close all
% clc
%
% savepath = 'D:\GoogleDrive\Research_from_laptop\sim_samp\images\newnewnewImages\';
%
% %RandStream.setDefaultStream(RandStream('mt19937ar','seed',1))
% rng(1);
% addpath('filter','data','navigators','samplingPatterns','ojwoodford-export_fig-5735e6d')
% %takes in a kspace image and an Nx3 long vector
%
% load data.mat
% load gy_index_cent
%
% groundTruth = ifftshift(ifftn(kspace_nav));
% imagesc(abs(rot90(rot90((groundTruth(:,:,20))))));
% colormap(gray)
% brighten(.5)
%
% N = length(gy_index_cent);
% %N =50;
% t = 1:N;
% f = .5;
% %y = -normpdf(t,N/2,N/50);
% %y = -sin(2*f*pi*t);
% y = zeros(size(t));
% z = (gy_index_cent-1)*.6; %sin(2*f*pi*t);
% x = zeros(size(t)); %sin(2*f*pi*t);
% % plot(t,y)
% % figure
% % plot(t,z)
%
% sig = 1e-7;
applyCorrection = 1;
% displayRunningInfo =0 ;
%
% ingate_params.ks = size(kspace_nav);
% ingate_params.spb = 33;
% ingate_params.wid = 6;
%
% outofgate_params.ks = size(kspace_nav);
% outofgate_params.spb = 33;
% outofgate_params.wid = 6;
%
% gate_params.y = y;
% gate_params.x = x;
% gate_params.z = z;
%
% gate_algo = @hardnav;
%
% dataCollection = [];
% titles = {}
% %%%%%%%%%%%%%%%%%%%%%
%
%
% % ingate_algo = @hacked_randGaussSamp_noverlap;
% % outofgate_algo = @randGaussSamp_overlap;
% %titleForSaveAndPlot = 'ifftrgsnover6_ifftrgsover6_avg_nocor';



%%%%%%%% AVG AND NAV
ingate_algo = @regSamp;
outofgate_algo = @holdSamp;
titleForSaveAndPlot = 'centric_centrichold_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @regSamp;
outofgate_algo = @ignoreSamp;
titleForSaveAndPlot = 'centric_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;



%%%%%%%% TO SHIFT OR NOT TO SHIFT
ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.wid = 4;
ingate_params.wid = 4;
outofgate_params.shift = 1;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_ifftrgsover4_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 1;
ingate_params.shift = 0;
titleForSaveAndPlot = 'rgsnover4_ifftrgsover4_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 0;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_rgsover4_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 0;
ingate_params.shift = 0;
titleForSaveAndPlot = 'rgsnover4_rgsover4_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;




%%%%%%%% DOES NOVERLAP WORK BETTER THEN NAV
ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
outofgate_params.shift = 0;
ingate_params.shift = 0;
titleForSaveAndPlot = 'rgsnover4_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
outofgate_params.shift = 0;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


%%%%%%%% NOVERLAP WIDTH
ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 1;
titleForSaveAndPlot = 'rgsnover1_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 2;
titleForSaveAndPlot = 'rgsnover2_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 3;
titleForSaveAndPlot = 'rgsnover3_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 4;
titleForSaveAndPlot = 'rgsnover4_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 5;
titleForSaveAndPlot = 'rgsnover5_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @randGaussSamp_noverlap;
outofgate_algo = @ignoreSamp;
ingate_params.wid = 6;
titleForSaveAndPlot = 'rgsnover4_centricignore_ignore_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;





%%%%%%%% CENTRIC + RGS (SHIFT?)
ingate_algo = @regSamp;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 1;
ingate_params.shift = 0;
titleForSaveAndPlot = 'centric_ifftrgsnover4_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;


ingate_algo = @regSamp;
outofgate_algo = @randGaussSamp_overlap;
outofgate_params.shift = 0;
ingate_params.shift = 0;
titleForSaveAndPlot = 'centric_rgsover4_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;



%%%%%%%% HACKED RGS WITH CENTRIC?
ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @holdSamp;
ingate_params.shift = 1;
titleForSaveAndPlot = 'ifftrgsnover4_centrichold_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;

ingate_algo = @hacked_randGaussSamp_noverlap;
outofgate_algo = @holdSamp;
ingate_params.shift =0 ;
titleForSaveAndPlot = 'rgsnover4_centrichold_avg_cor';
[sspace17, cspace17] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo);
recon1 = ifftshift(ifftn(sspace17));
[ blur, mse, ssim ] = printAndInfo( recon1, cspace17, groundTruth , savepath,titleForSaveAndPlot);
dataCollection(:,end+1) = [blur;mse;ssim;sum(sum(sum(cspace17)))]
titles{end+1} = titleForSaveAndPlot;



%%%%%%%% PLOT
metrics =  {'blur','mse','ssim','samples'}

for i=1:1:size(dataCollection,1)
    figure
    bar(dataCollection(i,:))
    set(gca, 'XTickLabel',titles, 'XTick',1:numel(titles))
    set(gca,'defaulttextinterpreter','none')
    rotateXLabels( gca, 45 )
    title(metrics{i})
end
