function [sspace cspaceToRet] = sim_samp(kspace_nav,ingate_algo,ingate_params,outofgate_algo,outofgate_params, gate_algo, gate_params, x, y, z, sig, applyCorrection, displayRunningInfo)

% kspace ,movementpattern, stationary sampling, movement sampling
% currentImage = ifftshift(ifftn(kspace_nav));
% fil_currentImage = ordfilt3D(currentImage,1);
% kspace_nav = fftn(fftshift(fil_currentImage));
%global kspace sspace cspace ks

rng(1)
takePics = 0;

global ssn msn tsm i 

N = length(x);
t = 1:N;
ks = size(kspace_nav);
kspace = kspace_nav;
sspace = zeros(ks);
in_aquis = zeros(ks);
out_aquis = zeros(ks);
cspace = zeros(ks);

ypsmat = ones(ks);
xpsmat = ones(ks);
zpsmat = ones(ks);

i = 1;
cont = 1;
ssn = 1;
msn = 1;
tsm = 1;
piccount = 1;

recordingCspace = zeros(198,59,354);
while cont
    
    ssn
    msn
    
    %with jitter
    %exp((1:1:size(curCircImg,2))*1i*randn(1)/jitter);
    yps(:,1,1) = exp(  (1:1:ks(1)).*(1i*y(i)) );
    xps(1,:,1) = exp(  (1:1:ks(2)).*(1i*x(i)) );
    zps(1,1,:) = exp(  (1:1:ks(3)).*(1i*z(i)) );
    ypsmat = repmat(yps,[1,ks(2),ks(3)]);
    xpsmat = repmat(xps,[ks(1),1,ks(3)]);
    zpsmat = repmat(zps,[ks(1),ks(2),1]);
    kspace = kspace_nav.*ypsmat.*xpsmat.*zpsmat + sig*complex(randn(ks),randn(ks));
    
    if gate_algo(gate_params)
        
        [samp,cont] = ingate_algo(ingate_params);
        cspace = cspace + samp;
        sspace(logical(samp)) = sspace(logical(samp)) + kspace(    logical(samp));
        fprintf('ingate (nsamps %f) \n',sum(sum(sum(samp))));
        
    else
        
        [samp,cont] = outofgate_algo(outofgate_params);
        
        
        if applyCorrection
            %for zshift = -.05:.01:.05
            zps(1,1,:) = exp(  (1:1:ks(3)).*(1i*-(z(i)+(randn*0.003))) );
            zpsmat = repmat(zps,[ks(1),ks(2),1]);
            
            %end
            cspace = cspace + samp;
            kspace_deshift = kspace.*zpsmat;
            sspace(logical(samp)) = sspace(logical(samp)) + kspace_deshift(    logical(samp));
            
            fprintf('out of gate corrected (nsamps %f) \n',sum(sum(sum(samp))));
        else
            cspace = cspace + samp;
            sspace(logical(samp)) = sspace(logical(samp)) + kspace(    logical(samp));
            
            fprintf('out of gate not corrected (nsamps %f) \n',sum(sum(sum(samp))));
        end
        
        
    end
   

   % recordingCspace(:,:,ssn) = squeeze(samp(1,:,:));
    
    
    if displayRunningInfo
        figure(1)
        h = imagesc(squeeze(cspace(1,:,:)));
        colormap(jet);
        
        
        if takePics
        saveas(h, strcat('./gif/sampPat',num2str(piccount)), 'png');
        piccount = piccount+1;
        end
        
        figure(2)
        plot(t,y,t,x,t,z)
        hold on
        scatter(t(i),y(i))
        hold off
        
        figure(3)
        workingRecon = ifftshift(ifftn(kspace));
        imagesc(abs(workingRecon(:,:,20)));
        colormap(gray);
        
        pause(.00001)
    end
    
    if size(cspace) ~= size(kspace_nav)
        error('myApp:argChk', 'Trying to sample outside of kspace, somthing might be wrong.  Check your ending conditions')
    end
    %we need to repeat this until enough samples have been taken
    %if sum( cspace == 0 ) == 0; cont = false; end
    if i == N;  i = 1; else i = i+1; end;
end

sspace( sspace ~= 0 ) = sspace(sspace ~= 0)./cspace(sspace ~= 0);
cspaceToRet = cspace;

end
