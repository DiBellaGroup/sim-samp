load avg_rung

figure
colormap gray
imagesc(abs(rot90(rot90(recon(:,:,20)))));
brighten(.4)
title('Averaged Rejected Recon')
sig = mean(mean(abs(rot90(rot90(recon(121:134,85:97,20))))))
meansig = mean(mean(mean(abs(recon))));

figure
colormap gray
imagesc(abs(rot90(rot90(recon_orig(:,:,20)))));
brighten(.4)
title('Original Recon (ground truth)')
mean(mean(abs(rot90(rot90(recon_orig(121:134,85:97,20))))))
mean(mean(mean(abs(recon_orig))))

load nav_rung
figure
colormap gray
imagesc(abs(rot90(rot90(recon(:,:,20)))));
brighten(.4)
title('Discard Rejected Recon')
mean(mean(abs(rot90(rot90(recon(121:134,85:97,20))))))
mean(mean(mean(abs(recon))))
% z=getframe(gcf)
% grayImage = rgb2gray(z.cdata)
% imwrite(grayImage,gray, 'yaya.png')